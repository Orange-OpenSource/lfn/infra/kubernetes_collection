#!/bin/bash

# SPDX-license-identifier: Apache-2.0
##############################################################################
# Copyright (c) 2018 Orange and others.
#
# All rights reserved. This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
# http://www.apache.org/licenses/LICENSE-2.0
##############################################################################

set -o errexit
set -o nounset
set -o pipefail

labels=$*

RUN_SCRIPT=${0}
RUN_ROOT=$(dirname $(readlink -f ${RUN_SCRIPT}))
export RUN_ROOT=$RUN_ROOT
source ${RUN_ROOT}/scripts/rc.sh

# register our handler
trap submit_bug_report ERR


#-------------------------------------------------------------------------------
# If no labels are set with args, run all
#-------------------------------------------------------------------------------
if [[ $labels = "" ]]; then
  labels="deploy_longhorn deploy_prometheus deploy_efk deploy_keycloak_operator"
fi

#-------------------------------------------------------------------------------
# Install centralized logging components
#  - install fluentd
#  - install elasticsearch
#  - install kibana
#-------------------------------------------------------------------------------
if [[ $labels = *"deploy_efk"* ]]; then
  step_banner "Install centralized logging components"
  ansible-playbook ${ANSIBLE_VERBOSE} \
    -i ${RUN_ROOT}/inventory/infra \
    ${RUN_ROOT}/playbooks/deploy_efk.yml  \
    --vault-id ${RUN_ROOT}/.vault
  step_banner "Centralized logging components installed"
fi

#-------------------------------------------------------------------------------
# Install Cert-Manager
#-------------------------------------------------------------------------------
if [[ $labels = *"deploy_certmanager"* ]]; then
  step_banner "Install Cert-Manager"
  ansible-playbook ${ANSIBLE_VERBOSE} \
    -i ${RUN_ROOT}/inventory/infra \
    ${RUN_ROOT}/playbooks/deploy_certmanager.yml  \
    --vault-id ${RUN_ROOT}/.vault
  step_banner "Cert-Manager installed"
fi

#-------------------------------------------------------------------------------
# Install Cinder
#-------------------------------------------------------------------------------
if [[ $labels = *"deploy_cinder"* ]]; then
  step_banner "Install Cinder"
  ansible-playbook ${ANSIBLE_VERBOSE} \
    -i ${RUN_ROOT}/inventory/infra \
    ${RUN_ROOT}/playbooks/deploy_cinder.yml  \
    --vault-id ${RUN_ROOT}/.vault
  step_banner "Cinder installed"
fi

#-------------------------------------------------------------------------------
# Install Longhorn
#-------------------------------------------------------------------------------
if [[ $labels = *"deploy_longhorn"* ]]; then
  step_banner "Install Longhorn"
  ansible-playbook ${ANSIBLE_VERBOSE} \
    -i ${RUN_ROOT}/inventory/infra \
    ${RUN_ROOT}/playbooks/deploy_longhorn.yml  \
    --vault-id ${RUN_ROOT}/.vault
  step_banner "Longhorn installed"
fi

#-------------------------------------------------------------------------------
# Install Prometheus
#-------------------------------------------------------------------------------
if [[ $labels = *"deploy_prometheus"* ]]; then
  step_banner "Install Prometheus"
  ansible-playbook ${ANSIBLE_VERBOSE} \
    -i ${RUN_ROOT}/inventory/infra \
    ${RUN_ROOT}/playbooks/deploy_prometheus.yml  \
    --vault-id ${RUN_ROOT}/.vault
  step_banner "Prometheus installed"
fi

#-------------------------------------------------------------------------------
# Install Keycloak
#-------------------------------------------------------------------------------
if [[ $labels = *"deploy_keycloak"* ]]; then
  step_banner "Install Keycloak"
  ansible-playbook ${ANSIBLE_VERBOSE} \
    -i ${RUN_ROOT}/inventory/infra \
    ${RUN_ROOT}/playbooks/deploy_keycloak.yml  \
    --vault-id ${RUN_ROOT}/.vault
  step_banner "Keycloak installed"
fi

#-------------------------------------------------------------------------------
# Install Keycloak operator
#-------------------------------------------------------------------------------
if [[ $labels = *"deploy_KeycloakOperator"* ]]; then
  step_banner "Install Keycloak operator"
  ansible-playbook ${ANSIBLE_VERBOSE} \
    -i ${RUN_ROOT}/inventory/infra \
    ${RUN_ROOT}/playbooks/deploy_keycloak_operator.yml  \
    --vault-id ${RUN_ROOT}/.vault
  step_banner "Keycloak operator installed"
fi

#-------------------------------------------------------------------------------
# test cert-manager installation
#-------------------------------------------------------------------------------
if [[ $labels = *"test_certmanager"* ]]; then
  step_banner "test that cert-manager is working"
  ansible-playbook ${ANSIBLE_VERBOSE} \
    -i ${RUN_ROOT}/inventory/infra \
    ${RUN_ROOT}/playbooks/test_certmanager.yml  \
    --vault-id ${RUN_ROOT}/.vault
  step_banner "Cert-Manager is working"
fi

#-------------------------------------------------------------------------------
# test cinder installation
#-------------------------------------------------------------------------------
if [[ $labels = *"test_cinder"* ]]; then
  step_banner "test that cinder is working"
  ansible-playbook ${ANSIBLE_VERBOSE} \
    -i ${RUN_ROOT}/inventory/infra \
    ${RUN_ROOT}/playbooks/test_cinder.yml  \
    --vault-id ${RUN_ROOT}/.vault
  step_banner "Cinder is working"
fi

#-------------------------------------------------------------------------------
# test elasticsearch installation
#-------------------------------------------------------------------------------
if [[ $labels = *"test_elasticsearch"* ]]; then
  step_banner "test that elasticsearch is working"
  ansible-playbook ${ANSIBLE_VERBOSE} \
    -i ${RUN_ROOT}/inventory/infra \
    ${RUN_ROOT}/playbooks/test_elastic.yml  \
    --vault-id ${RUN_ROOT}/.vault
  step_banner "Elasticsearch is working"
fi

#-------------------------------------------------------------------------------
# test fluentd installation
#-------------------------------------------------------------------------------
if [[ $labels = *"test_fluentd"* ]]; then
  step_banner "test that fluentd is working"
  ansible-playbook ${ANSIBLE_VERBOSE} \
    -i ${RUN_ROOT}/inventory/infra \
    ${RUN_ROOT}/playbooks/test_fluentd.yml  \
    --vault-id ${RUN_ROOT}/.vault
  step_banner "Fluentd is working"
fi

#-------------------------------------------------------------------------------
# test longhorn installation
#-------------------------------------------------------------------------------
if [[ $labels = *"test_longhorn"* ]]; then
  step_banner "test that longhorn is working"
  ansible-playbook ${ANSIBLE_VERBOSE} \
    -i ${RUN_ROOT}/inventory/infra \
    ${RUN_ROOT}/playbooks/test_longhorn.yml  \
    --vault-id ${RUN_ROOT}/.vault
  step_banner "Longhorn is working"
fi

#-------------------------------------------------------------------------------
# test prometheus installation
#-------------------------------------------------------------------------------
if [[ $labels = *"test_prometheus"* ]]; then
  step_banner "test that prometheus is working"
  ansible-playbook ${ANSIBLE_VERBOSE} \
    -i ${RUN_ROOT}/inventory/infra \
    ${RUN_ROOT}/playbooks/test_prometheus.yml  \
    --vault-id ${RUN_ROOT}/.vault
  step_banner "Prometheus is working"
fi

#-------------------------------------------------------------------------------
# test Keycloak installation
#-------------------------------------------------------------------------------
if [[ $labels = *"test_keycloak"* ]]; then
  step_banner "test that Keycloak is working"
  ansible-playbook ${ANSIBLE_VERBOSE} \
    -i ${RUN_ROOT}/inventory/infra \
    ${RUN_ROOT}/playbooks/test_keycloak.yml  \
    --vault-id ${RUN_ROOT}/.vault
  step_banner "Keycloak is working"
fi

#-------------------------------------------------------------------------------
# test Keycloak operator installation
#-------------------------------------------------------------------------------
if [[ $labels = *"test_KeycloakOperator"* ]]; then
  step_banner "test that Keycloak operator is working"
  ansible-playbook ${ANSIBLE_VERBOSE} \
    -i ${RUN_ROOT}/inventory/infra \
    ${RUN_ROOT}/playbooks/test_keycloak_operator.yml  \
    --vault-id ${RUN_ROOT}/.vault
  step_banner "Keycloak operator is working"
fi
