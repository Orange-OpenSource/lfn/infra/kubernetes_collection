# A collection for preparing hosts for K8S and installing platform components

This repo contains the `orange.k8s` Ansible Collection. The collection includes
roles dedicated to "PaaS" components installation on top of a running Kubernetes
but also role dedicated to prepare hosts for Kubernetes installation.

## External requirements

`external_requirements` role groups all external requirements.

This role needs privilege escalation.

## Collection dependencies

This collection uses `kubernetes.core` collection when dealing with
Kubernetes resources.

## Included content

The collection provides the following roles:

- external_requirements
- longhorn (storage class)
- elasticsearch
- fluentd
- prepare

## Using this collection

Before using the General community collection, you need to install the
collection with the `ansible-galaxy` CLI:

```shell
ansible-galaxy collection install https://gitlab.com/Orange-OpenSource/lfn/infra/kubernetes_collection.git
```

use `,branch_or_tag` if you want to use a specific branch version:

```shell
ansible-galaxy collection install https://gitlab.com/Orange-OpenSource/lfn/infra/kubernetes_collection.git,v1
```

You can also include it in a `requirements.yml` file and install it via
`ansible-galaxy collection install -r requirements.yml` using the format:

```yaml
collections:
  - name: git+https://gitlab.com/Orange-OpenSource/lfn/infra/kubernetes_collection.git
    type: git
    version: master
```
