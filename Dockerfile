FROM registry.gitlab.com/orange-opensource/lfn/ci_cd/docker-ansible-core:2.17@sha256:e43c38d8ced2bbb65c7ab1bb6c3618ba724dae770d428283f865657cd4ecf749

ARG CI_COMMIT_SHORT_SHA
ARG BUILD_DATE

LABEL org.opencontainers.image.source="https://gitlab.com/Orange-OpenSource/lfn/infra/kubernetes_collection" \
    org.opencontainers.image.ref.name="Kubernetes collection by Orange" \
    org.opencontainers.image.authors="Johan Deppe <johan.deppe@soprasteria.com> \
    Sylvain Desbureaux <sylvain.desbureaux@orange.com> \
    Jordan Desnoes <jordan.desnoes@soprasteria.com> \
    Othman Touijer <othman.touijer@soprasteria.com> \
    Nicolas Schlewitz <nicolas.schlewitz@orange.com>" \
    org.opencontainers.image.licenses=" Apache-2.0" \
    org.opencontainers.image.revision=$CI_COMMIT_SHORT_SHA

COPY ansible.cfg /etc/ansible/
ENV APP /opt/kubernetes/
ENV COLLECTION_PATH ~/.ansible/collections

WORKDIR $APP

COPY . $APP/

RUN mkdir -p "$COLLECTION_PATH" && \
    git config --global --add safe.directory "$PWD/.git" &&\
    ansible-galaxy collection install "git+file://$PWD/.git" \
      -p "$COLLECTION_PATH" && \
    rm -rf "$APP/.git" && \
    rm /root/.ansible/collections/ansible_collections/community/general/plugins/modules/java_keystore.py
