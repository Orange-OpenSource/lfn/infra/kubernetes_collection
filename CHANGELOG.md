## [9.1.11](https://gitlab.com/Orange-OpenSource/lfn/infra/kubernetes_collection/compare/9.1.10...9.1.11) (2025-02-16)


### Bug Fixes

* **deps:** update jaegertracing/jaeger-operator docker tag to v1.65.0 ([5c9704f](https://gitlab.com/Orange-OpenSource/lfn/infra/kubernetes_collection/commit/5c9704f2227d91aef61fab5246d973b8f572e993))

## [9.1.10](https://gitlab.com/Orange-OpenSource/lfn/infra/kubernetes_collection/compare/9.1.9...9.1.10) (2025-02-16)


### Bug Fixes

* **deps:** pin dependencies ([585b657](https://gitlab.com/Orange-OpenSource/lfn/infra/kubernetes_collection/commit/585b657385e3c97f490566af985eb2b019592bc9))

## [9.1.9](https://gitlab.com/Orange-OpenSource/lfn/infra/kubernetes_collection/compare/9.1.8...9.1.9) (2025-02-15)


### Bug Fixes

* **deps:** update dependency orange-opensource/lfn/infra/infra_collection to v9.0.37 ([5990338](https://gitlab.com/Orange-OpenSource/lfn/infra/kubernetes_collection/commit/5990338c6c01350765ff05c8ca43fddd96d23e6e))

## [9.1.8](https://gitlab.com/Orange-OpenSource/lfn/infra/kubernetes_collection/compare/9.1.7...9.1.8) (2025-02-15)


### Bug Fixes

* **deps:** update galaxy packages ([7d2b6c9](https://gitlab.com/Orange-OpenSource/lfn/infra/kubernetes_collection/commit/7d2b6c9b44c7f8ac8a283c1f30a0770c2f5312a7))

## [9.1.7](https://gitlab.com/Orange-OpenSource/lfn/infra/kubernetes_collection/compare/9.1.6...9.1.7) (2025-01-24)


### Bug Fixes

* **deps:** update dependency kubernetes to v32 ([396e7a7](https://gitlab.com/Orange-OpenSource/lfn/infra/kubernetes_collection/commit/396e7a7299a969816b9cab0a218a528e2bec4a72))

## [9.1.6](https://gitlab.com/Orange-OpenSource/lfn/infra/kubernetes_collection/compare/9.1.5...9.1.6) (2025-01-20)


### Bug Fixes

* **deps:** update dependency kubernetes.core to >=5.1.0,<5.2.0 ([a6f1cbd](https://gitlab.com/Orange-OpenSource/lfn/infra/kubernetes_collection/commit/a6f1cbd8b67387dbc3524e97c0412e9621097e44))

## [9.1.5](https://gitlab.com/Orange-OpenSource/lfn/infra/kubernetes_collection/compare/9.1.4...9.1.5) (2025-01-09)


### Bug Fixes

* **deps:** update galaxy packages ([bc62063](https://gitlab.com/Orange-OpenSource/lfn/infra/kubernetes_collection/commit/bc62063fa8a955620f1a9158ee8c2e2cc6fc667f))

## [9.1.4](https://gitlab.com/Orange-OpenSource/lfn/infra/kubernetes_collection/compare/9.1.3...9.1.4) (2024-12-17)


### Bug Fixes

* **deps:** update dependency orange-opensource/lfn/infra/infra_collection to v9.0.32 ([7a15439](https://gitlab.com/Orange-OpenSource/lfn/infra/kubernetes_collection/commit/7a154392c74cf02be39a70713dbe7670492c9ee2))

## [9.1.3](https://gitlab.com/Orange-OpenSource/lfn/infra/kubernetes_collection/compare/9.1.2...9.1.3) (2024-12-13)


### Bug Fixes

* **deps:** update galaxy packages ([5f37495](https://gitlab.com/Orange-OpenSource/lfn/infra/kubernetes_collection/commit/5f374950726a65c9e5401b237c31a854edc58234))

## [9.1.2](https://gitlab.com/Orange-OpenSource/lfn/infra/kubernetes_collection/compare/9.1.1...9.1.2) (2024-11-27)


### Bug Fixes

* **deps:** update dependency orange-opensource/lfn/infra/infra_collection to v9.0.27 ([f01fc32](https://gitlab.com/Orange-OpenSource/lfn/infra/kubernetes_collection/commit/f01fc3290a30b72da59ce0bf4b99a9875d8f7336))

## [9.1.1](https://gitlab.com/Orange-OpenSource/lfn/infra/kubernetes_collection/compare/9.1.0...9.1.1) (2024-10-21)


### Bug Fixes

* **deps:** update galaxy packages ([f31f811](https://gitlab.com/Orange-OpenSource/lfn/infra/kubernetes_collection/commit/f31f8113220f11ffe1195841ebeb790c5508c780))

# [9.1.0](https://gitlab.com/Orange-OpenSource/lfn/infra/kubernetes_collection/compare/9.0.34...9.1.0) (2024-10-21)


### Features

* **🔥:** remove unmaintained code ([6fddc67](https://gitlab.com/Orange-OpenSource/lfn/infra/kubernetes_collection/commit/6fddc670a281ae601d8415f20b380eb2a4db9cca))

## [9.0.34](https://gitlab.com/Orange-OpenSource/lfn/infra/kubernetes_collection/compare/9.0.33...9.0.34) (2024-09-26)


### Bug Fixes

* **deps:** update dependency kubernetes to v31 ([0aeeef2](https://gitlab.com/Orange-OpenSource/lfn/infra/kubernetes_collection/commit/0aeeef2d69cfd3abe3f9673b236f8204068c8bfb))

## [9.0.33](https://gitlab.com/Orange-OpenSource/lfn/infra/kubernetes_collection/compare/9.0.32...9.0.33) (2024-09-24)


### Bug Fixes

* **deps:** update galaxy packages ([e6248b2](https://gitlab.com/Orange-OpenSource/lfn/infra/kubernetes_collection/commit/e6248b22f17a2cc8993ffb2a1affd25ed5ceb617))

## [9.0.32](https://gitlab.com/Orange-OpenSource/lfn/infra/kubernetes_collection/compare/9.0.31...9.0.32) (2024-06-18)


### Bug Fixes

* **deps:** update galaxy packages ([96586d3](https://gitlab.com/Orange-OpenSource/lfn/infra/kubernetes_collection/commit/96586d3ee5c2f797b692b5742e9837e88e12fa4c))

## [9.0.31](https://gitlab.com/Orange-OpenSource/lfn/infra/kubernetes_collection/compare/9.0.30...9.0.31) (2024-06-14)


### Bug Fixes

* **deps:** update dependency kubernetes to v30 ([117df31](https://gitlab.com/Orange-OpenSource/lfn/infra/kubernetes_collection/commit/117df31a448b8a168a9976e9fb4cb4b72143e3ab))

## [9.0.30](https://gitlab.com/Orange-OpenSource/lfn/infra/kubernetes_collection/compare/9.0.29...9.0.30) (2024-06-12)


### Bug Fixes

* **deps:** update dependency kubernetes.core to v5 ([f79c289](https://gitlab.com/Orange-OpenSource/lfn/infra/kubernetes_collection/commit/f79c28980517a4842886a2d3684dbd004e91c707))

## [9.0.29](https://gitlab.com/Orange-OpenSource/lfn/infra/kubernetes_collection/compare/9.0.28...9.0.29) (2024-05-29)


### Bug Fixes

* **deps:** update dependency kubernetes.core to v4 ([ea87f57](https://gitlab.com/Orange-OpenSource/lfn/infra/kubernetes_collection/commit/ea87f5756e28bb7e7a36d7811d26fe57dd45fd6e))

## [9.0.28](https://gitlab.com/Orange-OpenSource/lfn/infra/kubernetes_collection/compare/9.0.27...9.0.28) (2024-05-27)


### Bug Fixes

* **deps:** update registry.gitlab.com/orange-opensource/lfn/ci_cd/docker-ansible-core docker tag to v2.17 ([ddf5176](https://gitlab.com/Orange-OpenSource/lfn/infra/kubernetes_collection/commit/ddf5176c6d16ad7800ec8ed063a17187cf90afee))

## [9.0.27](https://gitlab.com/Orange-OpenSource/lfn/infra/kubernetes_collection/compare/9.0.26...9.0.27) (2024-05-23)


### Bug Fixes

* **deps:** update dependency community.general to v9 ([53b82fa](https://gitlab.com/Orange-OpenSource/lfn/infra/kubernetes_collection/commit/53b82fa43f284342a334efb62d4dc738861b01bb))
* **deps:** update dependency orange-opensource/lfn/infra/infra_collection to v9.0.19 ([68cd858](https://gitlab.com/Orange-OpenSource/lfn/infra/kubernetes_collection/commit/68cd858b58892e08ec9cd883001e061d5e6a2b1e))

## [9.0.26](https://gitlab.com/Orange-OpenSource/lfn/infra/kubernetes_collection/compare/9.0.25...9.0.26) (2024-05-21)


### Bug Fixes

* **deps:** update dependency kubernetes.core to >=3.1.0,<3.2.0 ([707288f](https://gitlab.com/Orange-OpenSource/lfn/infra/kubernetes_collection/commit/707288f8b98473e7b2940692a3b116d6031a5ae1))

## [9.0.25](https://gitlab.com/Orange-OpenSource/lfn/infra/kubernetes_collection/compare/9.0.24...9.0.25) (2024-04-29)


### Bug Fixes

* **deps:** update galaxy packages ([ded1643](https://gitlab.com/Orange-OpenSource/lfn/infra/kubernetes_collection/commit/ded16438ff211fe74b063eea69942c8e53bd1a7e))

## [9.0.24](https://gitlab.com/Orange-OpenSource/lfn/infra/kubernetes_collection/compare/9.0.23...9.0.24) (2024-03-26)


### Bug Fixes

* **deps:** update galaxy packages ([1a61af0](https://gitlab.com/Orange-OpenSource/lfn/infra/kubernetes_collection/commit/1a61af0d4b04f49982f5a5d0ae3f788b6d0c0167))

## [9.0.23](https://gitlab.com/Orange-OpenSource/lfn/infra/kubernetes_collection/compare/9.0.22...9.0.23) (2024-03-13)


### Bug Fixes

* **deps:** update galaxy packages ([6375dff](https://gitlab.com/Orange-OpenSource/lfn/infra/kubernetes_collection/commit/6375dffc1f8224ff39e7265b97c097ab08c27772))

## [9.0.22](https://gitlab.com/Orange-OpenSource/lfn/infra/kubernetes_collection/compare/9.0.21...9.0.22) (2024-1-30)


### Bug Fixes

* **deps:** update galaxy packages ([6868b09](https://gitlab.com/Orange-OpenSource/lfn/infra/kubernetes_collection/commit/6868b098d90726779d7a888a7712c22e77ec5e58))

## [9.0.21](https://gitlab.com/Orange-OpenSource/lfn/infra/kubernetes_collection/compare/9.0.20...9.0.21) (2024-01-09)


### Bug Fixes

* **deps:** update dependency kubernetes to v29 ([643381c](https://gitlab.com/Orange-OpenSource/lfn/infra/kubernetes_collection/commit/643381ca3a2439836cf9dcb1aa8c38bf71770584))

## [9.0.20](https://gitlab.com/Orange-OpenSource/lfn/infra/kubernetes_collection/compare/9.0.19...9.0.20) (2024-01-09)


### Bug Fixes

* **deps:** update galaxy packages ([c7d34ca](https://gitlab.com/Orange-OpenSource/lfn/infra/kubernetes_collection/commit/c7d34caea8a997910ab97ffcf28cf27131880c2b))

## [9.0.19](https://gitlab.com/Orange-OpenSource/lfn/infra/kubernetes_collection/compare/9.0.18...9.0.19) (2023-12-05)


### Bug Fixes

* **deps:** update galaxy packages ([f66e598](https://gitlab.com/Orange-OpenSource/lfn/infra/kubernetes_collection/commit/f66e598d78dd7cb341074f0cd4119bbff3a37ae2))

## [9.0.18](https://gitlab.com/Orange-OpenSource/lfn/infra/kubernetes_collection/compare/9.0.17...9.0.18) (2023-11-22)


### Bug Fixes

* **deps:** update dependency kubernetes.core to v3 ([a42509a](https://gitlab.com/Orange-OpenSource/lfn/infra/kubernetes_collection/commit/a42509a23249c0da7cf56bf9897c801a54f2a2b9))

## [9.0.17](https://gitlab.com/Orange-OpenSource/lfn/infra/kubernetes_collection/compare/9.0.16...9.0.17) (2023-11-14)


### Bug Fixes

* **deps:** update galaxy packages ([e2fff86](https://gitlab.com/Orange-OpenSource/lfn/infra/kubernetes_collection/commit/e2fff86245e756f95c68c3b0223df28861f465e9))

## [9.0.16](https://gitlab.com/Orange-OpenSource/lfn/infra/kubernetes_collection/compare/9.0.15...9.0.16) (2023-11-13)


### Bug Fixes

* **deps:** update registry.gitlab.com/orange-opensource/lfn/ci_cd/docker-ansible-core docker tag to v2.16 ([e6e79fc](https://gitlab.com/Orange-OpenSource/lfn/infra/kubernetes_collection/commit/e6e79fc1c020f320201b8fc95f7b8476a56b7e03))

## [9.0.15](https://gitlab.com/Orange-OpenSource/lfn/infra/kubernetes_collection/compare/9.0.14...9.0.15) (2023-09-19)


### Bug Fixes

* **deps:** update dependency kubernetes to v28 ([7498604](https://gitlab.com/Orange-OpenSource/lfn/infra/kubernetes_collection/commit/7498604dbf7335094f53464a696c5d5728039c41))

## [9.0.14](https://gitlab.com/Orange-OpenSource/lfn/infra/kubernetes_collection/compare/9.0.13...9.0.14) (2023-09-13)


### Bug Fixes

* **deps:** update galaxy packages ([ead2d26](https://gitlab.com/Orange-OpenSource/lfn/infra/kubernetes_collection/commit/ead2d26dbd4f5d46b878196ad8b24e09caabc7ad))

## [9.0.13](https://gitlab.com/Orange-OpenSource/lfn/infra/kubernetes_collection/compare/9.0.12...9.0.13) (2023-08-31)


### Bug Fixes

* **deps:** update galaxy packages ([445fa09](https://gitlab.com/Orange-OpenSource/lfn/infra/kubernetes_collection/commit/445fa095e81fcd13426d1203a2df2c31a71cfbe9))

## [9.0.12](https://gitlab.com/Orange-OpenSource/lfn/infra/kubernetes_collection/compare/9.0.11...9.0.12) (2023-07-21)


### Bug Fixes

* **deps:** update dependency kubernetes to v27 ([5ab7707](https://gitlab.com/Orange-OpenSource/lfn/infra/kubernetes_collection/commit/5ab7707b70917120664d215cdb053efc961fdf1b))

## [9.0.11](https://gitlab.com/Orange-OpenSource/lfn/infra/kubernetes_collection/compare/9.0.10...9.0.11) (2023-07-18)


### Bug Fixes

* **deps:** update galaxy packages ([ca0fe38](https://gitlab.com/Orange-OpenSource/lfn/infra/kubernetes_collection/commit/ca0fe3871e883fbc3d32346108f3617464de7393))

## [9.0.10](https://gitlab.com/Orange-OpenSource/lfn/infra/kubernetes_collection/compare/9.0.9...9.0.10) (2023-05-16)


### Bug Fixes

* **deps:** update dependency orange-opensource/lfn/infra/infra_collection to v9.0.7 ([34b67a1](https://gitlab.com/Orange-OpenSource/lfn/infra/kubernetes_collection/commit/34b67a1cbeed6ab5b58c44043d528626d54c76b8))

## [9.0.9](https://gitlab.com/Orange-OpenSource/lfn/infra/kubernetes_collection/compare/9.0.8...9.0.9) (2023-05-06)


### Bug Fixes

* **deps:** update galaxy packages ([1649362](https://gitlab.com/Orange-OpenSource/lfn/infra/kubernetes_collection/commit/164936220bdd9a32972ca6a9a4dbc2c2f72f1dfb))

## [9.0.7](https://gitlab.com/Orange-OpenSource/lfn/infra/kubernetes_collection/compare/9.0.6...9.0.7) (2023-03-28)


### Bug Fixes

* **deps:** update galaxy packages ([e3dd895](https://gitlab.com/Orange-OpenSource/lfn/infra/kubernetes_collection/commit/e3dd8958e1be24fa67f7b4ca6f4ede33e3fc7bf4))

## [9.0.6](https://gitlab.com/Orange-OpenSource/lfn/infra/kubernetes_collection/compare/9.0.5...9.0.6) (2023-02-28)


### Bug Fixes

* **deps:** update galaxy packages ([c9899f0](https://gitlab.com/Orange-OpenSource/lfn/infra/kubernetes_collection/commit/c9899f00957480f70e48e42f88c8074175d89e92))

## [9.0.5](https://gitlab.com/Orange-OpenSource/lfn/infra/kubernetes_collection/compare/9.0.4...9.0.5) (2023-02-21)


### Bug Fixes

* **deps:** update dependency kubernetes to v26 ([15f701b](https://gitlab.com/Orange-OpenSource/lfn/infra/kubernetes_collection/commit/15f701b117993c770854b51713c960c50a221abc))

## [9.0.4](https://gitlab.com/Orange-OpenSource/lfn/infra/kubernetes_collection/compare/9.0.3...9.0.4) (2023-02-04)


### Bug Fixes

* **deps:** update galaxy packages ([6eec5719](https://gitlab.com/Orange-OpenSource/lfn/infra/kubernetes_collection/commit/6eec57195d65bedfccd15e9c5775995525cccc59))

  | datasource        | package                                      | from  | to    |
  | ----------------- | -------------------------------------------- | ----- | ----- |
  | gitlab-releases   | Orange-OpenSource/lfn/infra/infra_collection | 9.0.2 | 9.0.3 |
  | galaxy-collection | community.general                            | 6.2.0 | 6.3.0 |

## [9.0.3](https://gitlab.com/Orange-OpenSource/lfn/infra/kubernetes_collection/compare/9.0.2...9.0.3) (2023-02-03)


### Bug Fixes

* **deps:** update dependency kubernetes.core to >=2.4.0,<2.5.0 ([28e37a9](https://gitlab.com/Orange-OpenSource/lfn/infra/kubernetes_collection/commit/28e37a9d60723d80e7d3164929dead3d77d18d8c))

## [9.0.2](https://gitlab.com/Orange-OpenSource/lfn/infra/kubernetes_collection/compare/9.0.1...9.0.2) (2023-01-31)


### Bug Fixes

* **deps:** update python packages ([c297afe](https://gitlab.com/Orange-OpenSource/lfn/infra/kubernetes_collection/commit/c297afe158056c68c9bb0156a3fb6fccf97cb954))

## [9.0.1](https://gitlab.com/Orange-OpenSource/lfn/infra/kubernetes_collection/compare/9.0.0...9.0.1) (2023-01-31)


### Bug Fixes

* **deps:** update registry.gitlab.com/orange-opensource/lfn/ci_cd/docker-ansible-core docker tag to v2.14 ([c9087c2](https://gitlab.com/Orange-OpenSource/lfn/infra/kubernetes_collection/commit/c9087c2212e3e42923f73e41f3965f63c91ab272))
