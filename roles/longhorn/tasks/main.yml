---
- name: add longhorn repository
  kubernetes.core.helm_repository:
    name: longhorn
    repo_url: "{{ kubernetes_charts_longhorn_repository }}"
    repo_state: present

- name: create override directory
  ansible.builtin.file:
    path: "{{ kubernetes_charts_longhorn_etc_path }}"
    state: directory
    mode: 0700
    recurse: true

- name: create longhorn namespace
  run_once: true
  kubernetes.core.k8s:
    state: present
    definition:
      apiVersion: v1
      kind: Namespace
      metadata:
        name: "{{ kubernetes_charts_longhorn_namespace }}"
        labels:
          istio-injection: enabled
          name: "{{ kubernetes_charts_longhorn_namespace }}"

- name: generate longhorn override file
  ansible.builtin.template:
    src: overrides.yml.j2
    dest: "{{ longhorn_override_file }}"
    mode: 0600

- name: "install longhorn v{{ kubernetes_charts_longhorn_version }}"
  run_once: true
  kubernetes.core.helm:
    name: longhorn
    chart_ref: longhorn/longhorn
    namespace: "{{ kubernetes_charts_longhorn_namespace }}"
    values: "{{ lookup('template', 'overrides.yml.j2') | from_yaml }}"
    chart_version: "{{ kubernetes_charts_longhorn_version }}"

- name: wait for longhorn manager to be up and running
  run_once: true
  kubernetes.core.k8s_info:
    kind: DaemonSet
    wait: true
    name: longhorn-manager
    namespace: "{{ kubernetes_charts_longhorn_namespace }}"
    wait_sleep: 10
    wait_timeout: 1200

- name: wait for longhorn driver deployer to be up and running
  run_once: true
  kubernetes.core.k8s_info:
    kind: Deployment
    wait: true
    name: longhorn-driver-deployer
    namespace: "{{ kubernetes_charts_longhorn_namespace }}"
    wait_sleep: 10
    wait_timeout: 1200

- name: wait for longhorn csi-provisioner to be up and running
  run_once: true
  kubernetes.core.k8s_info:
    kind: Deployment
    wait: true
    name: csi-provisioner
    namespace: "{{ kubernetes_charts_longhorn_namespace }}"
    wait_sleep: 10
    wait_timeout: 1200
