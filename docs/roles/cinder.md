# Cinder

## Purpose

Install Cinder Storage Class (OpenStack Backend).
See [cinder documentation](
https://github.com/kubernetes/cloud-provider-openstack/blob/v1.22.0/README.md)
for a complete view of this component.

## Parameters

<!-- markdownlint-disable line-length -->
| Variable                                      | Purpose                                            | Default value                                           |
|-----------------------------------------------|----------------------------------------------------|---------------------------------------------------------|
| `base_dir`                                    | root folder of the collection                      | `"{{ inventory_dir }}/.."`                              |
| `kubernetes_charts_cinder_repository`         | helm repository to use                             | <https://kubernetes.github.io/cloud-provider-openstack> |
| `kubernetes_charts_cinder_etc_path`           | configuration directory                            | `{{ ansible_user_dir }}/helm/cinder`                    |
| `kubernetes_charts_cinder_namespace`          | namespace where to install                         | `cinder-system`                                         |
| `kubernetes_charts_cinder_version`            | chart version to use                               | 1.22.0                                                  |
| `kubernetes_charts_cinder_additional_classes` | number of replicas when creating volumes           | `[]`                                                    |
| `kubernetes_charts_cinder_default_class_name` | cinder storage class to be set as the default one  | Not Set                                                 |
| `kubernetes_docker_proxy`                     | proxy repository to use for image retrieval        | `docker.io`                                             |
| `kubernetes_k8s_gcr_proxy`                    | proxy repository to use for image retrieval        | `k8s.gcr.io`                                            |
| `kubernetes_kubelet_folder`                   | folder where kubelet will put its files            | `/var/lib/kubelet`                                      |
| `os_infra_local_cloud`                        | path of local cloud used by cinder to authenticate | `"{{ base_dir }}/vars/user_cloud.yml"`                  |
<!-- markdownlint-enable line-length -->

## Cinder storage classes

By default, two storage classes named `csi-cinder-sc-delete` and
`csi-cinder-sc-retain` will be created.

If other classes are needed, `kubernetes_charts_cinder_additional_classes` has
to be provided (see examples for actual use).

## Examples

```yaml
- ansible.builtin.import_role:
    name: orange.k8s.cinder

- ansible.builtin.import_role:
    name: orange.k8s.cinder
  vars:
    kubernetes_charts_cinder_version: 1.20.5
    kubernetes_charts_cinder_default_class_name: csi-cinder-sc-delete

# With additional classes
- ansible.builtin.import_role:
    name: orange.k8s.cinder
  vars:
    kubernetes_charts_cinder_additional_classes:
      - name: hdd
        parameters:
          availability: nova
          type: public
      - name: ssd
        parameters:
          availability: nova
          type: ssd
      - name: ssd-fast
        parameters:
          availability: nova
          type: ssd-fast
```
