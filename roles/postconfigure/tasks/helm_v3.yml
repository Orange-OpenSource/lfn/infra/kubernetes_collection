---
- name: add stable repo
  kubernetes.core.helm_repository:
    name: stable
    repo_url: https://charts.helm.sh/stable
    repo_state: present

- name: add helm repositories
  kubernetes.core.helm_repository:
    name: "{{ item.name }}"
    repo_url: "{{ item.url }}"
    repo_state: present
  loop: "{{ os_infra.kubernetes.helm.repositories }}"
  loop_control:
    label: "{{ item.name }}"
  when: os_infra.kubernetes is defined and
    os_infra.kubernetes.helm is defined and
    os_infra.kubernetes.helm.repositories is defined

- name: create helm configuration directory
  ansible.builtin.file:
    path: "{{ helm_etc_path }}"
    state: directory
    mode: '0644'
  when: os_infra.kubernetes is defined and
    os_infra.kubernetes.charts is defined and
    os_infra.kubernetes.charts | length > 0

- name: install charts except nfs-server-provisioner
  run_once: "yes"
  kubernetes.core.helm:
    name: "{{ item.key }}"
    chart_ref: "{{ item.value.chart }}"
    namespace: "{{ item.value.namespace | default('default') }}"
    values: "{{ item.value.content | from_yaml }}"
  loop: "{{ os_infra.kubernetes.charts | dict2items }}"
  loop_control:
    label: "{{ item.key }}"
  when: os_infra.kubernetes is defined and
    os_infra.kubernetes.charts is defined and
    os_infra.kubernetes.charts | length > 0 and
    item.key != "nfs-server-provisioner"

- name: install nfs-server-provisioner
  run_once: "yes"
  kubernetes.core.helm:
    name: nfs-server-provisioner
    chart_ref: stable/nfs-server-provisioner
    namespace: "{{ nfs_server_namespace }}"
    values: "{{ lookup('template', 'nfs-server-override.yaml.j2') |
      from_yaml }}"
  when: os_infra.kubernetes is defined and
    os_infra.kubernetes.charts is defined and
    os_infra.kubernetes.charts["nfs-server-provisioner"] is defined and
    os_infra.kubernetes.charts["nfs-server-provisioner"].enabled
