# Deploy longhorn playbook

## Purpose

Install external requirements and install longhorn as a storage class provider.

## Inventory

The inventory must provide a group `helm` where helm binary will be installed
and helm charts installation process will be started.
The inventory can also provide a group `k8s-cluster` where iSCSI daemon will be
started (it'll be also started on `helm` group's servers).

This playbook mandates ability to connect to the targeted Kubernetes via a kube
config file associated in default location
(`{{ ansible_user_dir }}/.kube/config`) for the chosen user on the server.

Search for vars files (`idf.yml` per default) happens at the same level as
inventory directory (see optional parameters to see how to change) per default:

```shell
.
├── inventory
│   └── inventory
└── vars
    └── idf.yml
```

## Optional parameters

On top of all variables from roles, the playbook adds the following variables:

| Variable        | Purpose                      | Default value            |
|-----------------|------------------------------|--------------------------|
| `property_file` | property file to load        | `idf.yml`. Optional      |
| `base_dir`      | property file to load folder | `{{ inventory_dir }}/..` |

## Tags

The playbook proposes the following tags:

| Tag              | Purpose                                                   |
|------------------|-----------------------------------------------------------|
| `longhorn`       | for controlling longhorn installation                     |
| `storage_class`  | for controlling storage class (via longhorn) installation |

## Examples

Inventory:

```ini
[helm]
my_server ansible_host: 1.2.3.4 ansible_user: toto
```

Launch:

```shell
ansible-playbook -i inventory/inventory playbooks/deploy_longhorn.yml

ansible-playbook -i inventory/inventory playbooks/deploy_longhorn.yml \
    --tags longhorn

ansible-playbook -i inventory/inventory playbooks/deploy_longhorn.yml \
    --extra-vars "base_dir=~"

ansible-playbook -i inventory/inventory playbooks/deploy_longhorn.yml \
    --extra-vars "{'kubernetes_docker_proxy': 'myProxy'}"
```
