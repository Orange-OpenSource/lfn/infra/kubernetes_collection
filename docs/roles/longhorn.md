# Longhorn

## Purpose

Install longhorn Storage Class.
See [longhorn documentation](https://longhorn.io/docs/) for a complete
view of this component.

## Parameters

<!-- markdownlint-disable line-length -->
| Variable                                              | Purpose                                             | Default value                          |
|-------------------------------------------------------|-----------------------------------------------------|----------------------------------------|
| `kubernetes_charts_longhorn_repository`               | helm repository to use                              | <https://charts.longhorn.io>           |
| `kubernetes_charts_longhorn_etc_path`                 | configuration directory                             | `{{ ansible_user_dir }}/helm/longhorn` |
| `kubernetes_charts_longhorn_namespace`                | namespace where to install                          | `longhorn-system`                      |
| `kubernetes_charts_longhorn_version`                  | chart version to use                                | 1.1.2                                  |
| `kubernetes_charts_longhorn_storage_replicas`         | number of replicas when creating volumes            | Not set                                |
| `kubernetes_charts_longhorn_storage_pod_deletion`     | what to do in case of pod deletion stuck            | Not set                                |
| `kubernetes_charts_longhorn_upgrade_check`            | check if a new version exists                       | `false`                                |
| `kubernetes_charts_longhorn_storage_min_avail`        | minimum available storage for PVC creation          | Not set                                |
| `kubernetes_charts_longhorn_storage_overprovisioning` | storage overprovisioning percentage                 | Not set                                |
| `kubernetes_charts_longhorn_default_class`            | is the `longhorn` storage class the default one     | `true`                                 |
| `kubernetes_docker_proxy`                             | proxy repository to use for image retrieval         | docker.io                              |
<!-- markdownlint-enable line-length -->

## Longhorn parameters

If not set, longhorn configuration parameters will be kept as default, except
for upgrade check (which is set to `false` per default).
See [longhorn documentation](
https://longhorn.io/docs/1.1.2/references/settings/) for the default value and
explanation on configuration usage.

## Examples

```yaml
- ansible.builtin.import_role:
    name: orange.k8s.longhorn

- ansible.builtin.import_role:
    name: orange.k8s.longhorn
  vars:
    kubernetes_charts_longhorn_version: 1.0.2
```
