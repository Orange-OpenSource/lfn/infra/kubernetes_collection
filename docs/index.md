
# Kubernetes platform collection

The goal of this collection consists in deploying
kubernetes addons simultaneously or separately.

It consists mainly in several playbooks used to deploy specific roles.
Each role corresponds to an add-on.

## Playbooks
<!-- markdownlint-disable line-length -->
| Playbook                                          | Description                                            |
|---------------------------------------------------|--------------------------------------------------------|
| [deploy_cinder](./playbooks/deploy_cinder.md)     | Playbook to deploy cinder storage class                |
| [deploy_longhorn](./playbooks/deploy_longhorn.md) | Playbook to deploy longhorn storage stack              |
| test_cinder                                       | Playbook use for the collection gating (cinder part)   |
| test_longhorn                                     | Playbook use for the collection gating (longhorn part) |
<!-- markdownlint-enable line-length -->

## Roles
<!-- markdownlint-disable line-length -->
| Role                                                      | Description                                       |
|-----------------------------------------------------------|---------------------------------------------------|
| [cinder](./roles/cinder.md)                               | Installation of cinder kubernetes storage class   |
| [longhorn](./roles/longhorn.md)                           | Installation of longhorn kubernetes storage class |
| [external requirements](./roles/external_requirements.md) | Management of external requirements               |
<!-- markdownlint-enable line-length -->

## Versions

| ORONAP version | Collection sha1 |
|----------------|-----------------|
| Honfleur       | N.A             |
| Istres         | N.A             |
