# Install needed external requirements

## Purpose

Install external requirements needed by the other roles of this collection.

iSCSI stack installation needs all servers from Kubernetes cluster.
Servers where helm will be installed must be in group given by variable
`k8s_helm_group` (`helm` per default).

## Parameters

<!-- markdownlint-disable no-inline-html line-length -->
| Variable                                | Purpose                                  | Default value                                                       |
|-----------------------------------------|------------------------------------------|---------------------------------------------------------------------|
| `python_pip_repository`                 | which pip repository to use              | `https://pypi.org/simple/`.                                         |
| `k8s_helm_group`                        | servers group where helm will be used    | helm                                                                |
| `kubernetes_charts_helm_repository`     | helm repository to use for helm binaries | <https://get.helm.sh>                                               |
| `kubernetes_charts_helm_version`        | the helm version to download             | v3.5.3.<br>⚠️ Give the new sha256 fingerprint if you change         |
| `kubernetes_charts_helm_archive_sha256` | the sha256 fingerprint of helm archive   | The one for v3.5.3.<br>⚠️ Give the new `helm_version` when changing |
<!-- markdownlint-enable no-inline-html line-length -->
## Examples

```yaml
- ansible.builtin.import_role:
    name: orange.k8s.external_requirements

- ansible.builtin.import_role:
    name: orange.k8s.external_requirements
  vars:
    python_pip_repository: http://myproxy/simple
```
